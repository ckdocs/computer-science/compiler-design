# Lexical Analysis (Scanner)

Get stream of program source code characters and devide it into `tokens` using `token classes` and send stream of tokens to `parser`

## Token (`<Type, Lexeme>`)

Pair of `token class` and `lexeme` (`substring` value of that token in program source) `(Token = <Class, Lexeme>)`

Lexical analyser receives a stream of `characters` and devide them into `tokens` and sends stream of tokens to `parser`

### Example

Input stream of characters:

```code
foo = 42
```

Output stream of tokens:

```code
<Identifier, "foo">
<Whitespace, " ">
<Operator, "=">
<Whitespace, " ">
<Number, "42">
```

---

### Token Class (Token Type)

Any types of words that a language can have

###### English

1. Noun
2. Verb
3. Adjective
4. etc

---

###### Programming language

Based on language these classes may be different

1. Keyword
2. Identifier
3. Integer
4. Whitespace
5. Delimiter
6. Operator

**Keyword**:

Any keyword of the language (Every language has a keyword table)

```code
else    => yes
if      => yes
then    => yes
fi      => yes
begin   => yes
```

---

**Identifier (Variable names)**:

String of letters or digits, starting with a letter (Or based on language grammers such as: `PHP => $xxx`)

```code
A1      => yes
Foo     => yes
bar     => yes
817     => no
b5@     => no
```

---

**Integer (Literal - Constants - Number)**:

A non-empty string of digits

```code
515     => yes
0b14    => yes
0x16    => yes
0a14b   => no
```

---

**Whitespace**:

A non-empty sequence of blanks, newlines, tabs, etc

**Tip**: Group all white spaces to getter (not in all languages: such as `python`)

```code
\t      => yes
\n      => yes
' '     => yes
\t\t\n  => yes
```

---

**Delimiter**:

Every specific single character class

```code
(       => yes
)       => yes
;       => yes
.       => yes
,       => yes
```

---

**Operator**:

Every specific mathematical or logical or etc operators

```code
=       => yes
==      => yes
+       => yes
*       => yes
/       => yes
```

---

### Token Lexeme

A substring in program source that lexer detects as a token called a `lexeme`

###### Regular Language

We use `Regular Languages` and `Regular Expressions` to specify `Token Classes` and their String Set (`Lexeme`)

**Keyword**:

```code
Keyword = "if" + "then" + "else" + "fi" + ...
```

---

**Integer (length > 1)**:

```code
Digit = "0" + "1" + "2" + ... + "9"
Integer = Digit.Digit* == Digit+
```

**Tip**: `AA*` == `A+`

---

**Identifier (Start with char)**:

```code
Digit = "0" + "1" + "2" + ... + "9"
Character = "a" + "b" + "c" + ... + "Z" == [a-zA-Z]
Identifier = Character.(Character + Digit)*
```

---

**Whitespace**:

```code
Space = " " + "\t" + "\n"
Whitespace = Space+
```

---

## Special Programming Languages

### Insignificant Whitespace

Some languages will remove every white space in the compile time

For example in `FORTRAN`:

```code
VAR1 = 15
VA R 1 = 15
```

---

### PL1 (IBM)

In some languages `Keywords` are not reserved and we can use them as `Identifiers`

```code
IF (ELSE) THEN
    THEN = ELSE
ELSE
    THEN = IF
FI
```

---

## Algorithms

> Read `automata theory tutorial` for algorithms details

```plantuml
@startuml
hide empty description

state "Regular Grammer" as Grammer
state "Regular Expression" as RegEX
state "NFA" as NFA
state "DFA" as DFA
state "NFA Table" as NFATable
state "DFA Table" as DFATable

Grammer --> RegEX
RegEX --> Grammer

RegEX --> NFA
NFA --> RegEX

NFA --> DFA
DFA --> NFA

NFA --> NFATable

DFA --> DFATable

@enduml
```

---

### RE vs FSM vs Table

```code
Regular Expression  => specification
Finit Automata      => implementation
Table               => more faster than FA
```

---

### NFA vs DFA

| DFA                | NFA                 |
| ------------------ | ------------------- |
| More memory needed | Lower memory needed |
| Faster speed       | Slower speed        |

---

### Lexer

Lexical Analysis has `1` main algorithm for `Lexing` tokens (spliting source code into tokens)

```js
Token[] lex(String source_code) {
    let regexes = get_all_tokens_regex();
    let nfas = regex_to_nfa(regexes);
    let dfas = nfa_to_dfa(nfas);
    let tables = dfa_to_tables(dfas);

    tables.order_by_highest_priority_token_class();

    string token = "";
    while(let symbol = input()){
        token += symbol;

        // if has acceptor table add next symbol (highest length token)
        for(let table of tables){
            if(table.accept(token)){
                continue;
            }
        }

        // now cannot accept that token
        let accepted_token = token.substring(0, token.length-2);

        // token is last symbol
        token = token[token.length-1];

        // detect class of accepted token
        for(let table of tables){
            if(table.accept(accepted_token)){
                // send <Class, accepted_token>
            }
        }
    }
}
```

1. Write all token classes `Regular Expressions`
    1. `Number = [0-9]+`
    2. `Keyword = "if" | "else" | ...`
    3. `Identifier = Char.(Char | Digit)*`
    4. `OpenPar = "("`
    5. etc
2. `R = Keyword | Identifier | ...`
3. Input `x1x2...xn`
4. For `i` in `1..n`
    1. If `x1..xi` in L(R)
        1. Remove `x1...xi` from string and goto 3

---

###### How much input

Accept `max length` of string that can accepted

**Example**:

```code
if (a == b){
    x = 5;
}


Accept "=" or "==" ?

Max length: "=="
```

---

###### Which token

Accept `highest priority` token

![Tokens Priorities](assets/tokens_priorities.jpg)

**Example**:

```code
if (a == b){
    x = 5;
}


Accept "if: Identifier" or "if: Keyword" ?

Highest priority: "if: Keyword"
```

---

###### No matches

Throw `error` syntax problem at line `x`

---

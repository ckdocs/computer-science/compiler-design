# Parsing (Syntax Analysis)

Get stream of `tokens` from `Lexer` and create the `Parse Tree`/`Abstract Syntax Tree` based on `Context Free Grammers` of programming language syntax

![Parser](assets/parser.png)

---

## Parse Tree

1. Terminals at the leaves
2. Non-Terminal at the interior nodes

An `in-order traversal` of the leaves is the orginal `input`

![Parse Tree](assets/parse_tree.png)

---

## Abstract Syntax Tree (Syntax Tree - `AST`)

-   Like parse tree
-   But ignore some details
-   Faster to use
-   Lower memory usage
-   Compilers mostly use `AST` instead of `Parse Tree`

![AST](assets/parse_tree_vs_ast.png)

---

## Error Handling

If our tokens are doesn't match to any grammer, compiler should `throw Errors`

### Error Types

Every levels of compiler has error type:

1. Lexical (Lexer)
2. Syntax (Parser)
3. Semantic (Checker)

![Error Types](assets/error_types.png)

---

### Error Handling Types

#### Panic mode

-   Simplest
-   Most popular
-   Synch tokens

If cannot find any grammer for receives token (`T[M,a] == NULL` or `T[M,a] == Not-Match`), `skip` lookahead forward and get `next token` and callback the `onError(line, error)`

##### Synch tokens

> All `Follow Set` tokens called synch token, whenever an error occurs, we iterate tokens until find the `first` synch token

##### Algorithm

![Panic Mode](assets/panic.png)

---

#### Phrase level

-   Use language grammers

Try replace error using language grammers

---

#### Error productions

-   Known mistakes
-   Complicated the grammer

Create some grammer for known mistakes that programmer may mistake

**Example**:

1. Write `5x` instead of `5*x`
2. Add production `E -> ... | EE`

---

#### Automatic local or global corrections

-   Try token insertions or deletions
-   Harder to implement (AI)
-   Slower compile time

**Example**:

`PL/C` language by IBM

---

## Context-Free Grammer

Now we remeber some tips about CFG's from `automata theory`

![CFL](assets/cfl.png)

### Ambigous Grammer

A grammer called `Ambigous` when have more than one `Parse Tree` or `Derivations` for an existed string.

#### Convert Ambigous to Disambigous (Manual)

When we define our programming language grammer it should not be ambigous, and user should programming codes disambigous

So

1. Define our ambigous grammer
2. Use online tools to convert to disambigous grammer

Or

use declaration rule `%left` and `%right` for `yacc` compiler compiler

---

### Recursive Grammer

A grammer called `Recursive` when we have a `loop` inside Non-Terminals `Dependency Graph`

#### Example

```code
A -> Bc
B -> Cf
C -> Ax
```

```plantuml
@startuml
hide empty description

state "A" as A
state "B" as B
state "C" as C

A --> B
B --> C
C --> A

@enduml
```

---

**Left Recursive**:

We called a grammer is `Left Recursive` when has a `Loop` and Non-Terminals of Loop are `left side of production`:

$$
\begin{aligned}
& A \xRightarrow{*} A \alpha
\end{aligned}
$$

**Direct Left Recursive**:

When a caller called self and loop is in one `Non-Terminal`

```code
A -> Aa | b
```

```plantuml
@startuml
hide empty description

state "A" as A

A --> A

@enduml
```

**Indirect Left Recursive**:

When a caller called another and loop is in more than one `Non-Terminals`

```code
A -> Bb
B -> A
```

```plantuml
@startuml
hide empty description

state "A" as A
state "B" as B

A --> B
B --> A

@enduml
```

---

**Right Recursive**:

We called a grammer is `Right Recursive` when has a `Loop` and Non-Terminals of Loop are `right side of production`:

$$
\begin{aligned}
& A \xRightarrow{*} \alpha A
\end{aligned}
$$

**Direct Right Recursive**:

When a caller called self and loop is in one `Non-Terminal`

```code
A -> aA | b
```

```plantuml
@startuml
hide empty description

state "A" as A

A --> A

@enduml
```

**Indirect Right Recursive**:

When a caller called another and loop is in more than one `Non-Terminals`

```code
A -> bB
B -> A
```

```plantuml
@startuml
hide empty description

state "A" as A
state "B" as B

A --> B
B --> A

@enduml
```

---

#### Remove Left Recursion

**Inputs** A grammar: a set of nonterminals ${\displaystyle A_{1},\ldots ,A_{n}}$ and `their productions`

**Output** A modified grammar generating the same language but `without left recursion`

##### Remove Direct Left Recursion

$$
\begin{aligned}
& A \rightarrow A\alpha_1 | A\alpha_2 | \dots | A\alpha_m | \beta_1 | \beta_2 | \dots | \beta_n
\\
& To:
\\
& A \rightarrow \beta_1A' | \beta_2A' | \dots | \beta_nA'
\\
& A' \rightarrow \alpha_1A' | \alpha_2A' | \dots | \alpha_mA'
\end{aligned}
$$

###### Example

```code
E   ->  E + T | T
T   ->  T * F | F
F   ->  (E) | id

to:

E   ->  TE'
E'  ->  + T E' | ε
T   ->  TT'
T'  ->  * F T' | ε
F   ->  (E) | id
```

---

##### Remove Indirect Left Recursion

1. Remove all `A -> ε` or `A ->+ A`
2. Numerize all productions in order
3. Per `Aj -> Ai r` if `j > i`
    1. Replace `Ai`
4. Remove `Direct Left Recursion`
5. Return `A -> ε` productions

###### Example

```code
S   ->  Aa | b
A   ->  Ac | Sd | ε

(1) Remove
S   ->  Aa | b
A   ->  Ac | Sd

(2) Numerize
X1  ->  X2a | b
X2  ->  X2c | X1d

(3) Replace
X1  ->  X2a | b
X2  ->  X2c | X2ad | bd

(4) Remove Direct
X1  ->  X2a | b
X2  ->  bdX
X   ->  cX | adX

(5) Return
X1  ->  X2a | b
X2  ->  bdX | X
X   ->  cX | adX | ε
```

---

### Refactoring Grammer

Is a technique to simplify grammer and factor unnecessary repated productions using `factorization method`.

#### Left Factored

Factoring from leftmost repeated `Terminal` and `Non-Terminal` combinations

$$
\begin{aligned}
& A \rightarrow \alpha \beta_{1} | \dots | \alpha \beta_{n} | \epsilon
\end{aligned}
$$

We will factor from $\alpha$ using defining new `Non-Terminal`:

$$
\begin{aligned}
& A \rightarrow \alpha B | \epsilon
\\
& B \rightarrow \beta_{1} | \dots | \beta_{n}
\end{aligned}
$$

**Example**:

$$
\begin{aligned}
Statement \rightarrow
\\
& | \: IF \: Expression \: THEN \: Statement
\\
& | \: IF \: Expression \: THEN \: Statement \: ELSE \: Statement
\\
& | \: a
\end{aligned}
$$

Refactored to:

$$
\begin{aligned}
Statement \rightarrow
\\
& | \: IF \: Expression \: THEN \: Statement \: X
\\
& | \: a
\\
\\
X \rightarrow
\\
& | \: ELSE \: Statement
\\
& | \: \epsilon
\end{aligned}
$$

---

#### Right Factored

> TODO

---

### Push-Down Automaton (PDA)

We discussed about `PDAs` at `automata theory`.

PDA has 2 parts:

1. FSM
2. Stack

#### Deteministic PDA (DPDA)

A `PDA` is `Deterministic` if and only if internal FSM is a type of `DFA` with `Deterministic Moves`, called `DPDA`.

**Tip**: We can convert `NFA` to `DFA`, but we cannot convert `NPDA` to `DPDA`, so languages generated by `DPDA`, `NPDA` are not same.

#### Nondeterministic PDA (NPDA)

A `PDA` is `Nondeterministic` if and only if internal FSM is a type of `NFA` with `Nondeterministic Moves`, called `NPDA`.

---

### Deterministic Grammer

A sub-category of `Context Free Grammers` called `Deterministic Context Free Grammers`) (DCFG) that can be generated by a `DPDA - Deterministic Push-Down Automaton`.

![DCFL](assets/dcfl.jpg)

-   **`Deterministic CFG` == `Disambigous CFG` == `LL(1) CFG`**
-   **`Ambigous CFG` => `Disambous CFG` using `Online Tools Convertors`**
-   **So every `CFG`, (DCFG, NCFG) can `convert` to `DCFG` form using `Hand` (Not-Algorithm)**

---

#### LL Grammer

A `Unambigous`, `Left factored` Context Free Grammer in `Greibach Normal Form - GNF` called `LL Grammer`:

$$
\begin{aligned}
& A \rightarrow aX
\\
\\
& X \in V^*
\\
& a \in T
\end{aligned}
$$

Algorithms:

1. `CFG` => `GNF`
2. `GNF` => `Left factoring` => `LL(K)`

These grammers are important in Parsers `without backtracking`.

**LL(K) Grammer**:

```code
LL(K):

L: Reading input string from `Left` to Right
L: `Leftmost` Derivation will use
K: K tokens lookahead (Must read `K` number of tokens to decide which production to select)

K means: at each step `K` choise of productions
```

---

**LL(1) Grammer Example**:

An `LL(K)` that isn't ambigous is `LL(1)` (`Deteministic CFG`)

**Tip**:

We disambigous every CFG, using `Online Tools` :)

Example:

string: `aaabd`

```code
S -> aA | bB
A -> aB | cB
B -> bC | aC
C -> bD
D -> d
```

Checking grammer is `LL(1)` or not ?

```code
accpet: a
current symbol: S
productions maybe: S -> aA

accept: a
current symbol: A
productions maybe: A -> aB

accept: a
current symbol: B
productions maybe: B -> aC

accept: b
current symbol: C
productions maybe: C -> bD

accept: d
current symbol: D
productions maybe: D -> d


Accepted
```

The max `accept` length is `1` so grammer can accept strings without any `backtracking`, (accept `one character` and `select` correct production)

---

**LL(2) Grammer Example**:

Every `LL(K)` grammer with `K > 1` is `Nondeteministic` (`has ambiguity`)(selecting of next production needed `more than one token` :))

string: `abd`

```code
S -> abB | aaA
B -> d
A -> c | d
```

Checking grammer is `LL(1)` or not ?

```code
accpet: a
current symbol: S
productions maybe: S -> abB | aaA

accpet: ab
current symbol: S
productions maybe: S -> abB

accpet: d
current symbol: B
productions maybe: B -> d

Accepted
```

The max `accept` length is `2` so grammer cannot accept strings without any `backtracking` (max => 2 production are maybe correct, parser selects one of them and then may be `backtracks`)

So Grammer is `LL(2)` (`Ambigous` - `Nondeterministic`)

We can convert it to `LL(1)` by `Disambigousing` it :)

---

#### LR Grammer

> TODO

---

**LR(K) Grammer**:

> TODO

---

**LR(0) Grammer**:

> TODO

---

**LR(1) Grammer**:

> TODO

---

## Parsing Table

> [!NOTE]
> There are two types of `Parsing Tables`:
>
> 1. LL Parsing Table
> 2. LR Parsing Table

---

> [!NOTE]
> Also there are main ideas about parsing tables:
>
> 1. First Set
> 2. Follow Set

---

### First Sets

> [!NOTE]
> See `left` side of production

Means the first `Terminal-Symbols` Set that can derived from `NonTerminal` X:

$$
\begin{aligned}
& First(X) = \{t | X \xrightarrow{*} t \alpha\} \cup \{\epsilon | X \xrightarrow{*} \epsilon\}
\end{aligned}
$$

#### Algorithm

```code
E   ->  aF | XYZT

(1)     First(`a`) = {`a`}

(2)     First(E) = {
            First(E): [Terminals]

            [First(X)]
            if (ε ∈ First(X)) {
                [First(Y)]
                if (ε ∈ First(Y)) {
                    [First(Z)]
                    if (ε ∈ First(Z)) {
                        [ε]
                    }
                }
            }
        }
```

![First Set](assets/first.jpg)

Example:

```code
E -> TX
T -> ( E ) | int Y
X -> + E | ε
Y -> * T | ε
```

First Sets:

```code
First(`+`) = {`+`}
First(`*`) = {`*`}
First(`(`) = {`(`}
First(`)`) = {`)`}
First(`int`) = {`int`}

First(E) = {
    [First(T)] = {`(`, `int`}
    X: if (ε ∈ First(T)) {
        [First(X)]
        if (ε ∈ First(X)) {
            [ε]
        }
    }
} = {`(`, `int`}

First(T) = {
    {`(`, `int`}
} = {`(`, `int`}

First(X) = {
    {`+`, ε}
} = {`+`, ε}

First(Y) = {
    {`*`, ε}
} = {`*`, ε}
```

---

### Follow Sets

> [!NOTE]
> See `right` side of production

Means the follow `Terminal-Symbols` Set that can derived after `NonTerminal` X:

$$
\begin{aligned}
& Follow(X) = \{t | S \xrightarrow{*} t \beta X t \delta\}
\end{aligned}
$$

#### Algorithm

```code
A -> XYZT

(1)     First(X) = {
            $: [X Is Start Symbol]

            [First(Y)]
            if (ε ∈ First(Y)) {
                [First(Z)]
                if (ε ∈ First(Z)) {
                    [First(T)]
                    if (ε ∈ First(T)) {
                        [Follow(A)]
                    }
                }
            }
        }
```

![Follow Set](assets/follow.jpg)

Example:

```code
E -> TX
T -> ( E ) | int Y
X -> + E | ε
Y -> * T | ε
```

Follow Sets:

```code
Follow(E) = {
    $

    First(`)`)

    Follow(X)
} = {$, `)`}

Follow(T) = {
    First(X) = {`+`, ε}
    if (ε ∈ First(X)) {
        Follow(E) = {$, `)`}
    }

    Follow(Y)
} = {$, `)`, `+`}

Follow(X) = {
    Follow(E)
} = {$, `)`}

Follow(Y) = {
    Follow(T)
} = {$, `)`, `+`}
```

---

### LL Parsing Table

![LL Parsing Table](assets/ll_parsing_table.jpg)

Is a table of `<NonTerminals, Terminals>` that we start parsing by `<S,input symbol>` and values of cells are productions that we can select per current `<NonTerminal, input>` pair.

#### Create

> Now we want to learn how to create `LL Parsing Table` for an existed `CFG`

1. Grammer `Unambiguous`
2. Grammer `Left Recursion` remove
3. Grammer `Left Factorize`
4. Find `First Set`
5. Find `Follow Set`

```code

T: clear table (rows: Non-Terminals, cols: Terminals)

foreach(A -> α: Production){
    foreach(t: First(α)){
        T[A,t] = α
    }

    if(ε ∈ First(α)){
        foreach(t: Follow(A)){
            T[A,t] = α
        }
    }

    if(ε ∈ First(α) && $ ∈ Follow(A)){
        T[A,$] = α
    }
}

```

![Parsing Table](assets/parsing_table.png)

---

#### Parse

![LL Parse Table](assets/ll_parse_table.jpg)

![LL Parse Table Example 1](assets/ll_parse_table_example1.jpg)

![LL Parse Table Example 2](assets/ll_parse_table_example2.jpg)

---

#### LL(K) Grammers Parsing Table

If `K > 1` then we have at-least one cell in the `Parse Table` with `more than 1` productions (`K` numbers) so if we go to that cell in Parsing process we cannot decide which `Production` to select

![LL(K) Parsing Table](assets/llk_parsing_table.png)

---

### LR Parsing Table

![LR Parsing Table](assets/lr_parsing_table.jpg)

#### Create

> Now we want to learn how to create `LR Parsing Table` for an existed `CFG`

1. Find `First Set`
2. Find `Follow Set`
3. Add `dummy production` (S' -> S)
4. `Numerize` productions
5. Find `NFA` of `closures`
6. Find `DFA` of `closures`
7. `Numerize` closures
8. Insert `goto` in table (DFA transactions by `Non-Terminal`)
9. Insert `shift` in table (DFA transactions by `Terminal`)
10. Insert `reduce` in table (LR(0) vs SLR(1) vs CLR(1) vs LALR(1))

##### LR(0)

Insert reduces by **all** `symbols`

---

##### SLR(1) => LR(0)

Insert reduces by **t ∈ Follow(X)** `symbols`

---

##### CLR(1) => LR(1)

Insert reduces by **lookahead** `symbols` (`S' -> S, $`)

###### Compute Lookahead

1. Start by `S' -> S, $`
2. If transition: lookahead `doesn't changes`
3. If sub non-terminal: lookahead `computes from super`

Example:

```code
S'  ->  .S,     $
S   ->  .Aa,    $
A   ->  .b,     a
```

---

##### LALR(1) => LR(1)

CLR + `merge same closures`

###### Same Closure

Closures that have same `productions` with different `lookaheads`

Example:

```code

A -> c.C, $
C -> .CC, $
C -> .d, $

and

A -> c.C, c/d
C -> .CC, c/d
C -> .d, c/d

merges to:

A -> c.C, c/d/$
C -> .CC, c/d/$
C -> .d, c/d/$

```

---

## Parsing Bottom-Up (Shift-Reduce)

These category of algorithms are `Bottom-Up`, with these basic Ideas:

1. Split string into two substrings
2. Right substring is as yet unexamined by parsing
3. Left substring has terminals and non-terminals
4. The dividing point is marked by `|`

$$
\begin{aligned}
& \alpha \: X \: | \: \omega
\\
\\
& \alpha \: X = Terminals, Nonterminals
\\
& \omega = Not parsed yet
\end{aligned}
$$

**Example**:

![Shift Reduce](assets/shift_reduce.png)

**Algorithm**:

-   `|`: Stack
-   `Shift`: Push left most token to stack
-   `Reduce`:
    -   Pop 0 or more from stack
    -   Try find production with poped tokens (`rhs`)
    -   Push production `left-side` (`lhs`)

---

### Production

Every grammer production:

```code
A -> BCD
```

---

### Reduction

Reverse of a production:

```code
BCD -> A
```

---

### Shift

**Shift**:

Move `|` one place to the `right`

```code
ABC|xyz => ABCx|yz
```

---

### Reduce

Try reduce `rightmost left` of `|` using productions

```code
A -> xy

Cbxy|ijk => CbA|ijk
```

---

### Shift-Reduce Conflict

When we `can shift or also reduce` and we don't know `what to do` (Not important problem)

Solving:

`Always try Reduce first` :)

---

### Reduce-Reduce Conflict

When we can `reduce` by `two different productions` and we don't know what to do (Bad problem)

---

### Handles

How we can decide when to `shift` or when to `reduce` ?

Example:

```code
E -> T + E | T
T -> int * T | int | (E)

int | * int + int


Shift or reduce ?

Let's Shift:
int * | int + int

It's OK


Let's Reduce:
T * int + int

We haven't any grammer `T*int` => blocked
It't Bad
```

---

So we want to Reduce if the result can still be reduces to the `Start Symbol`

$$
\begin{aligned}
& S \xrightarrow{*} \alpha X \omega \rightarrow \alpha \beta \omega
\end{aligned}
$$

**So $\alpha \beta$ is `Handle` of $\alpha \beta \omega$** (We can reduce it, and we gets to `S` start symbol)

-   Handles never goto Stack !!!

---

### Viable Prefix

$\alpha$ is a viable prefix if there is an $\omega$ such that $\alpha \: | \: \omega$ is a valid state of `Shift-Reduce Parser`

```code
int * int + int

T | * int + int => Bad
int * | int + int => Good

So

`T` => is not `Viable Prefix`
`int *` => is a `Viable Prefix`
```

**So for `Shift-Reduce` parsing we must keep the stack in a Valid `Viable Prefix` till it well converts to `S` start symbol** :)

#### Golden Key

**For any grammer the set of Viable Prefixes is a `Regular Language`**

So we can create an `FSM` for that set of Viable Prefix and we must check validity of Stack per every changes to keep it `Valid Viable Prefix` :)

---

### Items

Is a `Production` with a `.` somewhere on the `rhs(right hand side)`

Items of `T -> (E)`:

```code
T -> .(E)
T -> (.E)
T -> (E.)
T -> (E).
```

**Tip**:

Item for `X -> ε`:

```code
X -> .
```

these items often called `LR(0)` items.

Most of the time we haven't the entire `rhs` of a production in the `Stack` so we must compute the all `probable Productions` per every input and production

**Algorithm**:

Broke the stack tokens into `Prefix`es

```code
Token1 Token2 Token3 ... Tokenn

Prefix1 Prefix2 Prefix3 ... Prefixm
```

**Example**:

String: `(int*int)`
Shift-Reduce State: `(int* | int)`

```code
`(` = prefix of rhs: `T -> (E)`
`ε` = prefix of rhs: `E -> T`
`int*` = prefix of rhs: `T -> int*T`

stack of items:

T -> (.E)
E -> .T
T -> int*.T
```

---

### Viable Prefix NFA

Algorithms to generate `FSM` for validating `Viable Prefix`es of the `Stack` by iterating stack items from bottom to top

```code
NFA(stack) => yes | no
```

1. Add a dummy production: `S' -> S` to `G`
2. The `NFA States` are the `Items of G Productions`
3. For Every Item: `E -> α.Xβ`
    1. (X: `Terminal, NonTerminal`) Add transition: $(E \rightarrow \alpha \: . \: X \: \beta) \xrightarrow{X} (E \rightarrow \alpha \: X \: . \: \beta)$
    2. (X: `NonTerminal`) For Every Production: `X -> ω`
        1. Add transition: $(E \rightarrow \alpha \: . \: X \: \beta) \xrightarrow{\epsilon} (X \rightarrow . \: \omega)$
4. Every state is `Accepting State`
5. Start state is `S' -> .S`

---

#### Example

Grammer:

```code
S' -> E
E -> T+E | T
T -> int*T | int | (E)
```

Items: (NFA States)

```code
S' -> .E
S' -> E.

E -> .T+E
E -> T.+E
E -> T+.E
E -> T+E.

E -> .T
E -> T.

T -> .int*T
T -> int.*T
T -> int*.T
T -> int*T.

T -> .int
T -> int.

T -> .(E)
T -> (.E)
T -> (E.)
T -> (E).
```

Transitions:

```js
// for `S' -> .E` item
(S' -> .E) ->E (S' -> E.)
(S' -> .E) ->ε (E -> .T+E)
(S' -> .E) ->ε (E -> .T)

// for `S' -> E.` item

// for `E -> .T+E` item
(E -> .T+E) ->T (E -> T.+E)
(E -> .T+E) ->ε (E -> .int*T)
(E -> .T+E) ->ε (E -> .int)
(E -> .T+E) ->ε (E -> .(E))

// for `E -> T.+E` item
(E -> .T+E) ->+ (E -> T+.E)

// for `E -> T+.E` item
...
```

![Viable Prefix NFA](assets/viable_prefix_nfa.png)

Converting NFA to DFA:

![Viable Prefix DFA](assets/viable_prefix_dfa.png)

**Canonical Collections Of Items**:

The states of DFA (sets of items) called `Canonical Collections Of Items` or `Canonical Collections Of LR(0) Items`

---

### Valid Items (Valid sees)

Item $X \rightarrow \beta \: . \gamma$ is `Valid` for a Viable prefix $\alpha \: \beta$ if:

**After seeing $\alpha \: \beta$ seeing $\gamma$ is `Valid`**

$$
\begin{aligned}
S' \xrightarrow{*} \alpha \: X \: \omega \rightarrow \alpha \: \beta \: \gamma \: \omega
\end{aligned}
$$

**Defenition**:

**An item `I` is valid for a viable prefix `α` if the `DFA` recognizing viable prefixes terminates on input `α` in a state `S` containing `I`**

**Example**:

![Valid Item 1](assets/valid_item_1.png)
![Valid Item 2](assets/valid_item_2.png)

---

## Algorithms

### Top-Down

-   `Leftmost derivation`

#### Recursive Descent Parsing (Left-To-Right)

**Rules**:

1. Remove `Left-Recursion` from grammers
2. Write grammer in form `LL(K)`
3. Create `Parsing Table` for `LL(K)` grammer

**Pros and Cons**:

-   `Backtracking`, move backward when incorrect selects (`Lower Speed`)

**Algorithm**:

```js
AST parse(Token[] tokens){
    let token = tokens[0];

    for(let production of grammers){
        for(let symbol of production.symbols){
            if(symbol.isTerminal()){
                if(symbol.match(token)){
                    continue;
                }else{
                    break;
                }
            }else{
                production = symbol;
            }
        }
    }
}
```

1. Get token
2. Add token to array
3. Let Symbol be Start Symbol
4. For each production in Symbol
    1. If mach start production Symbol
    2. Get next Symbol and move to next symbol of production
    3. Else goto next production
    4. If hasn't any production
        1. Token can't match (error)

![Recursive Descent](assets/recursive_descent.png)

**Left-Recursive Problem**:

If we use a left-recursive CFG for Recursive Descent algorithm, it will fall in an `infinite loop`.

```code
S -> Pa
P -> Sx

S called P
P called S
S called P
P called S
...
```

![Left Recursive Problem](assets/recursive_descent_left_recursive_problem.png)

---

#### Recursive Descent Parsing (Recursive Functions - As Stack)

**Rules**:

1. Change grammer:
    1. Remove **`Ambiguity`**
    2. Remove **`Left Recursion`**
    3. Factor **`Left Factored`**
2. Create `DFA` for every grammer (`A new function`)
3. Start by calling start symbol's `function`
    1. If route using `Terminal` => `++cursor`, `pop stack`, `push End-Non-Terminal`
    2. If route using `Non-Terminal` => `push Non-Terminal`, `call Function`
    3. If route using `Epsillon` => `pop stack`, `push End-Non-Terminal`

**Example**:

Changed Grammer:

```code
E   ->  TE'
E'  ->  +TE' | ε
T   ->  FT'
T'  ->  *FT' | ε
F   -> (E) | id
```

DFA's of grammer:

```plantuml
@startuml
hide empty description

state E as "E :" {
    state E0 as "0"
    state E1 as "1"
    state E2 as "2" : **end**

    E0 --> E1 : **T**
    E1 --> E2 : **E'**
}

state EP as "E' :" {
    state EP0 as "0"
    state EP1 as "1"
    state EP2 as "2"
    state EP3 as "3" : **end**

    EP0 --> EP1 : **+**
    EP1 --> EP2 : **T**
    EP2 --> EP3 : **E'**

    EP0 --> EP3 : **ε**
}

state T as "T :" {
    state T0 as "0"
    state T1 as "1"
    state T2 as "2" : **end**

    T0 --> T1 : **F**
    T1 --> T2 : **T'**
}

state TP as "T' :" {
    state TP0 as "0"
    state TP1 as "1"
    state TP2 as "2"
    state TP3 as "3" : **end**

    TP0 --> TP1 : *****
    TP1 --> TP2 : **F**
    TP2 --> TP3 : **T'**

    TP0 --> TP3 : **ε**
}

state F as "F :" {
    state F0 as "0"
    state F1 as "1"
    state F2 as "2"
    state F3 as "3" : **end**

    F0 --> F1 : **(**
    F1 --> F2 : **E**
    F2 --> F3 : **)**

    F0 --> F3 : **id**
}

@enduml
```

Functions per DFA:

```js
AST parse(Token[] tokens){
    let cursor = 0;

    return E(cursor);
}

AST E(int cursor) {
    return T(cursor) && EP(cursor);
}

AST EP(int cursor) {
    return (tokens[cursor] == '+' && T(cursor) && EP(cursor)) || tokens[cursor] == 'ε';
}

...
```

---

#### Predictive Parsing (Left-To-Right)

**Rules**:

1. Change grammer:
    1. Remove **`Ambiguity`**
    2. Remove **`Left Recursion`**
    3. Factor **`Left Factored`**
    4. Write **`LL(1)`** form
2. Create `Parsing Table` for `LL(1)` grammer

**Props and Cons**:

-   `No backtracking` (predict next production) (Fastest Speed)

**Algorithm**:

```js
let stack = [S,$]

let table = createParseTable(...);

while(let item = stack.pop()){
    if(item.isTerminal()){
        if(item != nextToken()){
            throw new Error("Syntax Error");
        }
    }else{
        // existed production in that cell <NonTerminal, Terminal> => push production
        if(table[item, nextToken()]){
            stack.push(table[item, nextToken()]);
        }else{
            throw new Error("Syntax Error");
        }
    }
}

```

**Example**:

![Predictive Parser](assets/predictive_parser.png)

---

### Bottom-Up

-   `Right derivation`
-   More general than top-down parsers
-   All algorithms are types of `Shift-Reduce Parsings`

#### LR(0) Parsing

1. Stack contains `α`
2. Next input is `t`
3. Viable Prefix DFA on input `α` terminates in state `s`
4. `Reduce` by `X -> β` if:
    1. `s` contains item: `X -> β.`
5. `Shift` if:
    1. `s` contains item: `X -> β.tω`

---

**Reduce/Reduce Conflict**:

If any state of DFA has two reduce items:

1. `X -> β.`
2. `Y -> ω.`

---

**Shift/Reduce Conflict**:

If any state of DFA has one shift item and one reduce item:

1. `X -> β.`
2. `Y -> ω.tδ`

---

**Conflicts Example**:

![Conflict](assets/lr0_conflict.png)

**If there are conflicts in our grammer => our grammer is not `LR(0)`**

---

#### SLR(1) Parsing (Simple LR(0))

Improved LR(0) parser

1. Stack contains `α`
2. Next input is `t`
3. Viable Prefix DFA on input `α` terminates in state `s`
4. `Reduce` by `X -> β` if:
    1. `s` contains item: `X -> β.`
    2. **`t ∈ Follow(X)`** => Addition
5. `Shift` if:
    1. `s` contains item: `X -> β.tω`

**If there are conflicts in our grammer => our grammer is not `SLR(1)`**

![Conflict](assets/slr1_conflict.png)

---

**Main Algorithm**:

![Algorithm](assets/slr1_algorithm.png)

---

**Example**:

![Example](assets/slr1_example.png)

![Example 2](assets/slr1_example2.png)

---

**Optimization**:

Using `Memoization` save DFA states to prevent to start from begining of the stack after every restarts.

---

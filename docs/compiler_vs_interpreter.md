# Compiler vs Interpreter

![Compiler vs Interpreter](assets/compiler_vs_interpreter.png)

## Compiler

1. Generate `machine code`
2. Pass `data` to `machine code`
3. Read program text `more than one time`

### Pros and Cons

1. Slower compile time (read more than one time)
2. Faster run time

---

## Interpreter

1. Interprets `source code` with `data input`
2. Read program text `only one time`

### Pros and Cons

1. No compile time (read one time)
2. Slower run time

---

## History

### IBM 704 (1954)

1. Software > Hardware

### Speedcoding (1953)

1. By `John Backus`
2. 10x slower coding (low level language)

### FORTRAN 1 (1954 - 1957) - (Formula Translation)

1. By `John Backus`
2. First high level language
3. Modern compiler and languages use `Fortran`

#### FORTRAN compiler parts

1. Lexical Analysis
2. Parsing
3. Semantic Analysis
4. Optimization
5. Code Generation

Today for implementing new compiler, we will `re-implement` these parts with these defferents:

Old:

1. Lexical Analysis: **20%**
2. Parsing: **25%**
3. `Semantic Analysis`: **5%**
4. Optimization: **20%**
5. Code Generation: **25%**

Today:

1. Lexical Analysis: **5%**
2. Parsing: **10%**
3. `Semantic Analysis`: **40%**
4. `Optimization`: **40%**
5. Code Generation: **5%**

![History](assets/history.png)

---

**Lexical Analysis**:

Devide program text into `words` or `tokens`

![History Lexical Analysis 1](assets/history_lexical_analysis_1.png)
![History Lexical Analysis 2](assets/history_lexical_analysis_2.png)

**Parsing**:

`Tree` Diagramming sentences

![History Parsing 1](assets/history_parsing_1.png)
![History Parsing 2](assets/history_parsing_2.png)

**Semantic Analysis**: (`Hard`)

Try to understand meaning (Meaning Errors :))

For example

1. Redefine variable
2. Devide by zero
3. Number of function arguments and caller
4. Variables type checking

![History Semantic Analysis 1](assets/history_semantic_analysis_1.png)
![History Semantic Analysis 2](assets/history_semantic_analysis_2.png)

**Optimization**:

Change some part of source code for optimization:

1. Lower Memory Usage
2. Faster Speed

Example:

```code
int a = x * 0;
```

Optimized to:

```code
int a = 0;
```

**Code Generation**: (`usually assembly`)

Or to another language

1. Human language
2. Another programming language
3. etc

---

# Basics

> Compiler is an application that compiles a `source language` to a `destination language` and `throws` compiling time `errors`.

They has two phases:

1. Analysis (Creating `Syntax Tree`)
    1. Lexical Analyser
    2. Parser (Syntax Analyser)
    3. Semantic Analyser
2. Synthesis (Generation `Destination Source Code`)
    1. Optimization
    2. Code Generation

---

# Compiler Design

Is an application that compiles a `source language` to a `destination language` and `throws` compiling time `errors`.

```plantuml
@startuml
hide empty description

state "Lexical Analyser" as Lexer
state "Parser" as Parser
state "Semantic Analyser" as Checker

Lexer --> Parser : Stream of **Tokens**
Parser --> Checker : Parse **Tree/Syntax** Tree (**AST**)

@enduml
```

---

# Index

1. Basics

    1. Compiler Phases
        1. Analysis
        2. Synthesis

2. Compiler vs Interpreter

    1. Compiler
    2. Interpreter
    3. History
        1. IBM 704
        2. Speedcoding
        3. FORTRAN 1
            1. Lexical Analysis (Scanner)
            2. Parsing (Syntax Analysis)
            3. Semantic Analysis (Type Checking)
            4. Optimization
            5. Code Generation

3. Lexical Analysis (Scanner)

    1. Token (`<Type, Lexeme>`)
        1. Token Class (Token Type)
            1. English
            2. Programming Language
                1. Keyword
                2. Identifier
                3. Integer
                4. Whitespace
                5. Delimiter
                6. Operator
        2. Token Lexeme
            1. Regular Language (`RE`)
                1. Keyword
                2. Identifier
                3. Integer
                4. Whitespace
                5. Delimiter
                6. Operator
    2. Special Programming Languages
        1. Insignificant Whitespace
        2. PL1 (IBM)
    3. Algorithms
        1. RE vs FSM vs Table
        2. NFA vs DFA
        3. Lexer
            1. How much input (`max length`)
            2. Which token (`highest priority`)
            3. No matches (`throw error`)

4. Parsing (Syntax Analysis)

    1. Parse Tree
    2. Abstract Syntax Tree (Syntax Tree - `AST`)
    3. Error Handling
        1. Error Types
            1. Lexical
            2. Syntax
            3. Semantic
        2. Error Handling Types
            1. Panic mode
            2. Phrase level
            3. Error productions
            4. Automatic local or global corrections
    4. Context-Free Grammer
        1. Ambigous Grammer
            1. Convert Ambigous to Disambigous (Manual)
        2. Recursive Grammer
            1. Left Recursive
                1. Direct Left Recursive
                2. Indirect Left Recursive
            2. Right Recursive
                1. Direct Right Recursive
                2. Indirect Right Recursive
            3. Remove Direct/Indirect Left Recursion
        3. Refactoring Grammer
            1. Left Factored
            2. Right Factored
        4. Push-Down Automaton (PDA)
            1. Deteministic PDA (DPDA)
            2. Nondeterministic PDA (NPDA)
        5. Deterministic Grammer
            1. Tips (`NCFG` => `DCFG`) (`LL(K)` => `LL(1)`) (`Ambigous` => `Disambigous`)
                1. **`Deterministic CFG` == `Disambigous CFG` == `LL(1) CFG`**
                2. **`Ambigous CFG` => `Disambous CFG` using `Online Tools Convertors`**
                3. **So every `CFG`, (DCFG, NCFG) can `convert` to `DCFG` form using `Hand` (Not-Algorithm)**
            2. LL Grammer (`Unambigous` + `LeftFactored`)
                1. LL(K) Grammer
                2. LL(1) Grammer Example
                3. LL(2) Grammer Example
            3. LR Grammer
                1. LR(K) Grammer
                2. LR(0) Grammer
                3. LR(1) Grammer
    5. Parsing Table
        1. First Sets
        2. Follow Sets
        3. CFG => Parsing Table
        4. LL(K) Grammers Parsing Table
    6. Parsing Bottom-Up (Shift-Reduce)
        1. Production
        2. Reduction
        3. Shift
        4. Reduce
        5. Shift-Reduce Conflict
        6. Reduce-Reduce Conflict
        7. Handles (We can Reduce it)
        8. Viable Prefix
            1. **`Golden Key`**
        9. Items
        10. Viable Prefix NFA
            1. Canonical Collections Of Items
        11. Valid Items (`Valid sees`)
    7. Algorithms
        1. Top-Down
            1. Recursive Descent Parsing (Left-To-Right)
                1. Left-Recursive Problem
            2. Predictive Parsing (Left-To-Right)
        2. Bottom-Up
            1. LR(0) Parsing
            2. SLR(1) Parsing (Simple LR(0))
                1. Optimization

5. Semantic Analysis (Type Checking)

6. Optimization

7. Code Generation

---

# Lex/Yac

Tools for generating **Compilers**

```mermaid
graph TD;

LEXER[Lexical Analyzer];
PARSER[Syntax Analyzer];
CHECKER[Semantic Analyzer / Code Generation];

LEXER --"Stream of tokens"--> PARSER;
PARSER --"Parse Tree/Syntax Tree (AST)"--> CHECKER;
```

![Compile](assets/compile.gif)

---

## Lex

A `lexical analyzers` generator that uses `Regular Expressions (Patterns)`

### flex

An OpenSource implementation of `lex`

```code
sudo apt install flex
lex myLang.l
```

---

### Action (reduction)

A piece of `C` code that runs after a `Token` is recognized using that `Regular Expression`

```code
[a-z]*      {printf("text");}
```

---

### Source body

Every lex program has **Three** parts:

```c
Lex/C Definitions

%%

Patterns/Actions

%%

C Functions
```

---

#### Lex/C Definitions

1. `C` header includes
2. `C` functions declarations
3. `C` global variables
4. ...

```c
%{
#include <stdio.h>
void f();
int x;
%}
...

%%

Patterns

%%

C Functions
```

---

#### Patterns/Actions

A pattern and action pair, that actions runs when a token matches the pattern

```code
[a-z]*          {printf("text");}
[0-9]*          {printf("number");}
```

---

#### C Functions

1. `C` functions implementations

```code
void f() {
    printf("runs f()");
    return 1;
}
int yywrap (void) {
    return 1;
}
```

---

## Yacc

A `syntax analyzers (parser)` generator that uses `LALR(1) unambigous Grammers`

### bison

An OpenSource implementation of `yacc`

```code
sudo apt install bison
yacc -d myLang.y
```

---

### Action (reduction)

A piece of `C` code that runs after a `Reduction` is recognized

```code
E : A '+' B      {printf("text");}
```

---

### Attribute (union)

Every `Terminal` and `Non-Terminal` has a **`variable`** that changes during the Syntax Tree Traversal

---

#### Attributes union

We must define `all` the `attributes` as a package in `union` form

```code
%union {    // Attribute package
    int x;      // An attribute can be `int`
    char y;     // An attribute can be `char`
    float z;    // An attribute can be `float`
}
```

---

#### Attributes bindings

We can assign an `attribute of a specific type` to a `Terminal` or `Non-Terminal`

##### Terminals (Token)

```code
%token <x> NUM  // token with name `NUM` has attribute of type `int` named `x`
%token <y> ID   // token with name `ID` has attribute of type `char` named `y`
```

##### Non-Terminals (Variables)

```code
%type <x> line  // variable with name `line` has attribute of type `int` named `x`
%type <y> a b c   // variables with names `a`, `b`, `c` has attribute of type `char` named `y`
```

---

#### Attributes access in `Yacc` Actions

We can access `Attribute` of `Terminal` or `Non-Terminal` in every Yacc `Action` using `$x` symbol

```code
A : B '+' C         {$$ = $1 + $2;}
A : `log(` C `)`    {$$ = Math.log($2);}
```

---

#### Attributes access in `Lex` Actions

We can access `Attribute` of `Terminals` in every Lex `Action` using `yylval` and `yytext`

```code
[0-9]*      {yylval.x = atoi(yytext);}
[a-z]       {printf(yytext); yylval.y = yytext[0];}
```

---

### Source body

Every yacc program has **Three** parts:

```c
Yacc/C Definitions

%%

Grammers/Actions

%%

C Functions
```

---

#### Yacc/C Definitions

1. `C` header includes
2. `C` functions declarations
3. `C` global variables
4. `Yacc` attributes union
5. `Yacc` grammer start symbol
6. `Yacc` terminals definitions (`Attr` binding)
7. `Yacc` non-terminals definitions (`Attr` binding)
8. ...

```c
%{
#include <stdio.h>
void f();
int x;
%}

%union {
    int a;
    float b;
}
%start line
%token <a> INT_NUM
%token <b> FLOAT_NUM
%type <a> line
%type x y z
...

%%

Grammers

%%

C Functions
```

---

#### Grammers/Actions

A production and action pair, that actions runs when a reduction recognizes

```code
line :  INT_NUM          {$$ = $1;}
    |   line '+' INT_NUM {$$ = $1 + $3;}
```

---

#### C Functions

1. `C` functions implementations

```code
void f() {
    printf("runs f()");
    return 1;
}

int main (void) {
    int i;
    for(i=0; i<52; i++) {
        symbols[i] = 0;
    }

    return yyparse ();
}

void yyerror (char *s) {
    fprintf (stderr, "%s\n", s);
}
```

---

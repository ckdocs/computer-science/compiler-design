# Compiler Design

![SDLC](https://img.shields.io/badge/SDLC-Agile-informational)
![Date](https://img.shields.io/badge/BeginDate-10/6/2020-success)

Compiler Design

---

## Dependencies

1. `docker`

## Building

```sh
docker run --rm -it -v "$(pwd):/docs" squidfunk/mkdocs-material build
```

## Starting

```sh
docker run --rm -it -p 8000:8000 -v "$(pwd):/docs" squidfunk/mkdocs-material
```

---

## License

This project is licensed under the [MIT license](LICENSE.md).  
Copyright (c) KoLiBer (koliberr136a1@gmail.com)
